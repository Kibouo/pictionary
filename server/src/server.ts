import dotenv from "dotenv";
import express from "express";
import http from "http";
import https from "https";
import websocket from "websocket";
import fs from "fs";
import url from "url";

dotenv.config();

const TEAM_ID = 3;
const PORT_WEBSOCKET = 9000 + TEAM_ID;
const STATIC_DIR = "../client/dist";
const PRIVATE_KEY = process.env["SECURE"] !== "false" && fs.readFileSync('../server.key', 'utf8');
const CERTIFICATE = process.env["SECURE"] !== "false" && fs.readFileSync('../server.crt', 'utf8');

abstract class Server {
    private app: express.Application;
    private http_server: http.Server | https.Server;
    protected ws_server: websocket.server;

    constructor() {
        this.app = express();
        if (process.env["SECURE"] === "false") {
            console.warn("NOT secure HTTP server being used!");
            this.http_server = http.createServer(this.app);
        } else {
            this.http_server = https.createServer({ key: PRIVATE_KEY, cert: CERTIFICATE }, this.app);
        }
        this.ws_server = new websocket.server({
            httpServer: this.http_server,
            autoAcceptConnections: false,
        });

        this.app.use(express.static(STATIC_DIR));
        this.app.use((_req, res, next) => {
            res.header("Access-Control-Allow-Origin", process.env["WS_HOST"]);
            next();
        });

        this.ws_server.on("request", (req) => this.handle_ws_request(req));

        this.http_server.listen(PORT_WEBSOCKET);
        console.log(`Listening on port ${PORT_WEBSOCKET}`);
    }

    protected origin_allowed(request: websocket.request): boolean {
        return url.parse(request.origin).host === process.env["WS_HOST"];
    }

    abstract handle_ws_request(request: websocket.request): void;
}

export { Server };