import websocket from "websocket";
import { v4 as uuid4 } from "uuid";
import { Uuid } from "../../lib/src/data/uuid";
import { GroupInfo } from "../../lib/src/data/group";

class ConnectionHandler {
    private client_connections: Map<string, websocket.connection> = new Map();

    register(connection: websocket.connection): Uuid {
        const uuid = Uuid.parse(uuid4());
        this.client_connections.set(uuid.value, connection);
        return uuid;
    }

    remove(uuid: Uuid): void {
        this.client_connections.delete(uuid.value);
    }

    // Returns a connection if the uuid connection exists and if it is still open.
    get_connection(uuid: Uuid): websocket.connection | null {
        const connection = this.client_connections.get(uuid.value);
        if (connection === undefined) {
            return null;
        } else if (!connection.connected) {
            this.client_connections.delete(uuid.value);
            return null;
        } else {
            return connection;
        }
    }

    send_group(group: GroupInfo, message: string): void {
        group.players
            .map(player => this.get_connection(player.uuid))
            .filter(connection => connection !== null)
            .forEach(connection => connection.send(message));
    }
}

export { ConnectionHandler };