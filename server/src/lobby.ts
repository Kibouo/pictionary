import {
    UnregisteredGroup,
    GroupInfo,
    MIN_PLAYERS as MIN_PLAYERS_IN_GROUP
} from '../../lib/src/data/group';
import { Uuid } from '../../lib/src/data/uuid';
import { LobbyDiff } from '../../lib/src/action/lobby_list';
import { Player } from '../../lib/src/data/player';
import { SUCCESS_ACTION } from '../../lib/src/data/status_reply';

type GroupUuid = Uuid;
type PlayerUuid = Uuid;
// JS' `Map.has()` works by object comparison, we want value comparison tho so we can't use `Uuid`
type GroupUuidValue = string;
type PlayerUuidValue = string;

// Handles games in prep phase
class Lobby {
    private new_groups: Set<GroupUuidValue> = new Set();
    private removed_groups: GroupUuid[] = [];

    private _groups: Map<GroupUuidValue, GroupInfo> = new Map();
    private _players: Map<PlayerUuidValue, GroupUuid> = new Map();

    private ready_players: Set<PlayerUuidValue> = new Set();

    private add_group(group: GroupInfo): void {
        if (!this._groups.has(group.uuid.value)) {
            this.new_groups.add(group.uuid.value);
            this._groups.set(group.uuid.value, group);


            group.players
                .map(player => player.uuid.value)
                .forEach(player => this._players.set(player, group.uuid));
        }
    }

    private delete_group(group: GroupInfo): void {
        if (this._groups.has(group.uuid.value)) {
            this.removed_groups.push(group.uuid);
            this._groups.delete(group.uuid.value);


            group.players
                .map(player => player.uuid.value)
                .forEach(player => this._players.delete(player));
        }
    }

    private update_group(group: GroupInfo): void {
        this.delete_group(group);
        this.add_group(group);
    }

    peek_current_groups(): LobbyDiff {
        // Remove the items in `new_groups` as the next periodic update will 
        // send these anyway. If we would not remove these, the items in 
        // `new_groups` would arrive 2x at the client.
        return {
            added: [...this._groups]
                .filter(entry => !this.new_groups.has(entry[0]))
                .map(entry => entry[1]),
            deleted: []
        };
    }

    take_group_diff(): LobbyDiff {
        const diff = {
            added: [...this.new_groups]
                .map(uuid => this._groups.get(uuid)),
            deleted: this.removed_groups
        };

        this.new_groups.clear();
        this.removed_groups = [];

        return diff;
    }

    get players(): PlayerUuid[] {
        return [...this._players].map(([uuid, _]) => Uuid.parse(uuid));
    }

    get groups(): GroupInfo[] {
        return [...this._groups].map(entry => entry[1]);
    }

    group_by_uuid(uuid: GroupUuid): GroupInfo | null {
        return this._groups.get(uuid.value);
    }

    group_by_player(player_uuid: PlayerUuid): GroupInfo | null {
        const group_uuid = this._players.get(player_uuid.value);
        return group_uuid && this._groups.get(group_uuid.value);
    }

    // If a `player_uuid` of `null` is passed, no admin check will be performed.
    can_start_game(group_uuid: GroupUuid, player_uuid: PlayerUuid = null): string {
        const group = this.group_by_uuid(group_uuid);
        if (!group) {
            return "Group is not waiting in the lobby.";
        }
        if (player_uuid && !group.is_leader(player_uuid)) {
            return "Only the group leader can start a game.";
        }
        if (group.other_players.length + 1 < MIN_PLAYERS_IN_GROUP
            || group.other_players.length + 1 > group.max_players) {
            return `Member count doesn't satisfy constraints:
            - members: ${group.other_players.length + 1}
            - minimum: ${MIN_PLAYERS_IN_GROUP}
            - maximum: ${group.max_players}`;
        }

        return SUCCESS_ACTION;
    }

    /// Tries to create a group. Returns the Uuid as well when successfull.
    try_group_enter(group: UnregisteredGroup, player_uuid: PlayerUuid): string | Uuid {
        if (!this.players.map(uuid => uuid.value).includes(player_uuid.value)) {
            return "Player not waiting in lobby.";
        }
        if (this.group_by_player(player_uuid)) {
            return "Player already in a group.";
        }

        const registered_group: GroupInfo = new GroupInfo(
            group.name,
            {
                name: group.leader,
                uuid: player_uuid
            },
            group.max_players,
            group.game_config
        );

        this.add_group(registered_group);
        return registered_group.uuid;
    }

    // Disbanding the group leaves the players in the lobby, but removes their group-relation.
    try_group_leave(player_uuid: PlayerUuid, disband = true): string {
        if (!this.players.map(uuid => uuid.value).includes(player_uuid.value)) {
            return "Player not waiting in lobby.";
        }

        const group = this.group_by_player(player_uuid);
        if (!group) {
            return "Player not in a group.";
        }

        if (!group.is_leader(player_uuid)) {
            return "Only the group leader can make a group leave.";
        }

        this.delete_group(group);
        if (disband) {
            group.players
                .map(player => player.uuid.value)
                .forEach(player => this._players.set(player, null));
        }
        return SUCCESS_ACTION;
    }

    try_player_join_group(player: Player, group_uuid: GroupUuid): string {
        if (!this.players.map(uuid => uuid.value).includes(player.uuid.value)) {
            return "Player not waiting in lobby.";
        }

        const group = this.group_by_uuid(group_uuid);
        if (!group) {
            return "Requested group not waiting in lobby.";
        }

        if (!group.add_player(player)) {
            return "Group is full or player already joined.";
        }

        this.update_group(group);
        return SUCCESS_ACTION;
    }

    // // NOTE: works but unneeded
    // try_player_leave_group(player_uuid: PlayerUuid): string {
    //     if (!this.players.map(uuid => uuid.value).includes(player_uuid.value)) {
    //         return "Player not waiting in lobby.";
    //     }

    //     const group = this.group_by_player(player_uuid);
    //     if (!group) {
    //         return "Player not in a group.";
    //     }

    //     if (!group.remove_player(player_uuid)) {
    //         return "Admin cannot abandon group. Disband it instead.";
    //     }

    //     this.update_group(group);
    //     return SUCCESS_ACTION;
    // }

    try_player_enter(uuid: PlayerUuid): string {
        if (this.players.map(uuid => uuid.value).includes(uuid.value)) {
            return "Player already waiting in lobby.";
        }

        this._players.set(uuid.value, null);
        return SUCCESS_ACTION;
    }

    force_player_leave(uuid: PlayerUuid): void {
        const group = this.group_by_player(uuid);
        if (group?.leader.uuid.value === uuid.value) {
            this.delete_group(group);
            group.other_players
                .map(player => player.uuid.value)
                .forEach(player => this._players.set(player, null));
        } else if (group?.remove_player(uuid)) {
            this.update_group(group);
        } else {
            this._players.delete(uuid.value);
        }
    }

    // Returns whether game can be started
    mark_player_ready(player_uuid: PlayerUuid): boolean {
        this.ready_players.add(player_uuid.value);

        const group = this.group_by_player(player_uuid);
        if (!group) {
            return false;
        }

        return group.players.every(player => this.ready_players.has(player.uuid.value));
    }
}

export { LobbyDiff, Lobby };