import websocket from "websocket";
import { Lobby } from "./lobby";
import { ConnectionHandler } from "./connection_handler";
import { ACTION as LOBBY_JOIN_ACTION } from "../../lib/src/action/lobby_join";
import { Uuid } from "../../lib/src/data/uuid";
import { Server } from "./server";
import { lobby_list_msg } from "../../lib/src/action/lobby_list";
import {
    ACTION as GROUP_CREATE_ACTION,
    GroupCreate,
    group_create_reply
} from "../../lib/src/action/group_create";
import { GameHall } from "./game_hall";
import {
    ACTION as GROUP_JOIN_ACTION,
    GroupJoin,
    group_join_reply
} from "../../lib/src/action/group_join";
import { ACTION as GAME_INIT_ACTION, game_init_msg, game_init_reply } from "../../lib/src/action/game_init";
import { ACTION as SDP_OFFER_INIT_ACTION, SdpOfferInit } from "../../lib/src/action/sdp_offer_init";
import { SUCCESS_ACTION } from "../../lib/src/data/status_reply";
import { sdp_handshake_reply } from "../../lib/src/action/sdp_handshake";
import { sdp_offer_pass_msg } from "../../lib/src/action/sdp_offer_pass";
import { ACTION as SDP_REPLY_INIT_ACTION, SdpReplyInit } from "../../lib/src/action/sdp_reply_init";
import { sdp_reply_pass_msg } from "../../lib/src/action/sdp_reply_pass";
import { ACTION as ICE_CANDIDATE_INIT_ACTION, IceCandidateInit } from "../../lib/src/action/ice_candidate_init";
import { ice_candidate_pass_msg } from "../../lib/src/action/ice_candidate_pass";
import { ACTION as GAME_START_ACTION, GameStart } from "../../lib/src/action/game_start";
import { ACTION as GAME_GUESS_WORD_ACTION } from "../../lib/src/action/game_guess_word";

const LOBBY_UPDATE_SEC = 1;

class GameServer extends Server {
    private lobby = new Lobby();
    private connection_handler = new ConnectionHandler();
    private game_hall = new GameHall(this.connection_handler);

    constructor() {
        super();
    }

    // Begin pushing updates to the lobby.
    start_periodic_push_lobby_diff_to_client(): void {
        setInterval(() => {
            const diff = this.lobby.take_group_diff();

            for (const conn_uuid of this.lobby.players) {
                const connection = this.connection_handler.get_connection(conn_uuid);
                try {
                    connection.send(JSON.stringify(lobby_list_msg(diff)));
                } catch (e) {
                    this.connection_handler.remove(conn_uuid);
                }
            }
        }, LOBBY_UPDATE_SEC * 1000);
    }

    // Handle a incoming connection.
    handle_ws_request(request: websocket.request): void {
        if (!this.origin_allowed(request)) {
            request.reject();
            console.error(`Attempted connection from origin ${request.origin}`);
            return;
        }

        const connection = request.accept(null, request.origin);
        const connection_uuid = this.connection_handler.register(connection);

        connection.on("message", (message) => this.handle_message(connection, message, connection_uuid));

        connection.on("error", (error) => console.error(error));
        connection.on("close", () => {
            this.connection_handler.remove(connection_uuid);
            this.lobby.force_player_leave(connection_uuid);
        });
    }

    // Process an incoming message
    private handle_message(connection: websocket.connection, message: websocket.IMessage, uuid: Uuid): void {
        if (message.type === "binary") {
            console.log("bin data");
        } else if (message.type === "utf8") {
            const contents = JSON.parse(message.utf8Data);

            switch (contents.action) {
                case LOBBY_JOIN_ACTION:
                    this.lobby_join_action(connection, uuid);
                    break;

                case GROUP_CREATE_ACTION:
                    this.group_create_action(connection, uuid, contents);
                    break;

                case GROUP_JOIN_ACTION:
                    this.group_join_action(connection, uuid, contents);
                    break;

                case GAME_INIT_ACTION:
                    this.game_init_action(connection, uuid);
                    break;

                case SDP_OFFER_INIT_ACTION:
                    this.sdp_offer_init_action(connection, uuid, contents);
                    break;

                case SDP_REPLY_INIT_ACTION:
                    this.sdp_reply_init_action(connection, uuid, contents);
                    break;

                case ICE_CANDIDATE_INIT_ACTION:
                    this.ice_candidate_init_action(connection, uuid, contents);
                    break;

                case GAME_START_ACTION:
                    this.game_start_action(connection, uuid, contents);
                    break;

                case GAME_GUESS_WORD_ACTION:
                    this.game_hall.handle_incoming_guess(uuid, contents);
                    break;

                default:
                    connection.emit('error', new Error(`Invalid API action. Full data: ${JSON.stringify(contents)}`));
            }
        } else {
            connection.emit('error', new Error(`Invalid message type. Full message: ${message}`));
        }
    }

    // Let a player join the lobby
    private lobby_join_action(connection: websocket.connection, player_uuid: Uuid): void {
        connection.send(JSON.stringify(
            lobby_list_msg(this.lobby.peek_current_groups(), this.lobby.try_player_enter(player_uuid))));
    }

    // Lets a player create a new group
    private group_create_action(connection: websocket.connection, player_uuid: Uuid, contents: GroupCreate): void {
        const result = this.lobby.try_group_enter(contents.data.group, player_uuid);

        if (typeof result === "string") {
            connection.send(JSON.stringify(
                group_create_reply(result)));
        } else {
            connection.send(JSON.stringify(
                group_create_reply(SUCCESS_ACTION, result, player_uuid)));
        }
    }

    private group_join_action(connection: websocket.connection, player_uuid: Uuid, contents: GroupJoin): void {
        const result = this.lobby.try_player_join_group(
            { name: contents.data.player_name, uuid: player_uuid },
            contents.data.group_uuid
        );
        connection.send(JSON.stringify(group_join_reply(result, player_uuid)));
    }

    private game_init_action(connection: websocket.connection, player_uuid: Uuid): void {
        const group = this.lobby.group_by_player(player_uuid);
        const result = this.lobby.can_start_game(group.uuid, player_uuid);

        if (result !== SUCCESS_ACTION) {
            connection.send(JSON.stringify(game_init_reply(result)));
            return;
        }

        this.connection_handler.send_group(group, JSON.stringify(game_init_msg()));
    }

    private sdp_offer_init_action(connection: websocket.connection, player_uuid: Uuid, contents: SdpOfferInit): void {
        const group = this.lobby.group_by_player(player_uuid);

        if (!group) {
            connection.send(JSON.stringify(sdp_handshake_reply("Player not part of group, or the group is not waiting in the lobby.")));
            return;
        }

        const all_members = group.players.map(player => player.uuid.value);
        const members_in_msg = [player_uuid.value]
            .concat(contents.data.descriptions.map(descr => descr.target_user.value));
        if (!members_in_msg.every(msg_member => all_members.includes(msg_member))) {
            const message = "Some target client is not part of the source client's group.";
            connection.send(JSON.stringify(sdp_handshake_reply(message)));
            return;
        }

        const result = this.lobby.can_start_game(group.uuid);
        if (result !== SUCCESS_ACTION) {
            connection.send(JSON.stringify(sdp_handshake_reply(result)));
            return;
        }

        // Actually send the offers now that eligibility checks have passed
        const other_members = members_in_msg
            .filter(uuid => uuid !== player_uuid.value)
            .map(uuid =>
                [uuid, this.connection_handler.get_connection(Uuid.parse(uuid))])
            .filter(([_, connection]) => connection !== null);
        for (const member of other_members) {
            const member_uuid = member[0] as string;
            const member_connection = member[1] as websocket.connection;

            member_connection.send(JSON.stringify(sdp_offer_pass_msg(
                contents.data.descriptions.find(desc =>
                    desc.target_user.value === member_uuid).content,
                player_uuid
            )));
        }
    }

    private sdp_reply_init_action(connection: websocket.connection, player_uuid: Uuid, contents: SdpReplyInit): void {
        const group = this.lobby.group_by_player(player_uuid);

        if (!group) {
            connection.send(JSON.stringify(sdp_handshake_reply("Group is not waiting in the lobby.")));
            return;
        }

        const all_members: string[] = group.players.map(player => player.uuid.value);
        if (!all_members.includes(contents.data.target_user.value)) {
            const message = "SDP source and target client do not share a group.";
            connection.send(JSON.stringify(sdp_handshake_reply(message)));
            return;
        }

        const result = this.lobby.can_start_game(group.uuid);
        if (result !== SUCCESS_ACTION) {
            connection.send(JSON.stringify(sdp_handshake_reply(result)));
            return;
        }

        const target_connection = this.connection_handler.get_connection(contents.data.target_user);
        if (!target_connection) {
            const message = "Server cannot contact requested target.";
            connection.send(JSON.stringify(sdp_handshake_reply(message)));
            return;
        }

        // Pass on reply to offer
        target_connection.send(JSON.stringify(
            sdp_reply_pass_msg(contents.data.description, player_uuid)));
    }

    private ice_candidate_init_action(connection: websocket.connection, player_uuid: Uuid, contents: IceCandidateInit): void {
        // Perform same-group checks again. Just to be sure ¯\_(ツ)_/¯
        const group = this.lobby.group_by_player(player_uuid);

        if (!group) {
            connection.send(JSON.stringify(sdp_handshake_reply("Group is not waiting in the lobby.")));
            return;
        }

        const all_members: string[] = group.players.map(player => player.uuid.value);
        if (!all_members.includes(contents.data.target_user.value)) {
            const message = "SDP source and target client do not share a group.";
            connection.send(JSON.stringify(sdp_handshake_reply(message)));
            return;
        }

        const result = this.lobby.can_start_game(group.uuid);
        if (result !== SUCCESS_ACTION) {
            connection.send(JSON.stringify(sdp_handshake_reply(result)));
            return;
        }

        const target_connection = this.connection_handler.get_connection(contents.data.target_user);
        if (!target_connection) {
            const message = "Server cannot contact requested target.";
            connection.send(JSON.stringify(sdp_handshake_reply(message)));
            return;
        }

        // Pass on reply to offer
        target_connection.send(JSON.stringify(
            ice_candidate_pass_msg(contents.data.candidate, player_uuid)));
    }

    private game_start_action(connection: websocket.connection, player_uuid: Uuid, contents: GameStart): void {
        const group = this.lobby.group_by_player(player_uuid);
        // Group update was deleted, so these messages should be replaced.
        if (!group) {
            // TODO     connection.send(JSON.stringify(
            //  game_update_msg("No group to play with. Did not set ready status.")));
        } else if (contents.data.info !== SUCCESS_ACTION) {
            //todo    this.connection_handler.send_group(group, JSON.stringify(
            //        game_update_msg(contents.data.info)));
        } else if (!this.lobby.mark_player_ready(player_uuid)) {
            //todo  this.connection_handler.send_group(group, JSON.stringify(
            //todo      game_update_msg("Not everyone is ready yet.")));
        } else {
            const result = this.lobby.can_start_game(group.uuid);
            if (result !== SUCCESS_ACTION) {
                //todo: this.connection_handler.send_group(group, JSON.stringify(game_update_msg(result)));
            } else {
                const result = this.lobby.try_group_leave(group.leader.uuid, false);
                if (result === SUCCESS_ACTION) {
                    this.game_hall.new_game(group);
                } else {
                    // Couldn't leave group. TODO : notification?
                }
            }
        }
    }
}

export { GameServer };