import { GroupInfo } from "../../lib/src/data/group";
import { default_settings } from "../../lib/src/data/game";
import { GameHost } from "./GameHost";
import { Uuid } from "../../lib/src/data/uuid";
import { GameGuessWordRequest } from "../../lib/src/action/game_guess_word";
import { ConnectionHandler } from "./connection_handler";

type GameUuid = Uuid;
type PlayerUuid = Uuid;
// JS' `Map.has()` works by object comparison, we want value comparison tho so we can't use `Uuid`
type GameUuidValue = string;
type PlayerUuidValue = string;

// Handles ongoing games
class GameHall {
    private games: Map<GameUuidValue, GameHost> = new Map();
    private players: Map<PlayerUuidValue, GameUuid> = new Map();
    private connection: ConnectionHandler;

    public constructor(connection: ConnectionHandler) {
        this.connection = connection;
    }

    private game_by_player(uuid: PlayerUuidValue): GameHost | null {
        const key = this.players.get(uuid);
        if (key !== undefined) {
            const game = this.games.get(key.value);
            if (game === undefined) {
                return null;
            } else {
                return game;
            }
        }
    }

    public new_game(group: GroupInfo): void {
        const game_host = new GameHost(group.game_config, group, this.connection);
        this.games.set(group.uuid.value, game_host);

        group.players.forEach(pl =>
            this.players.set(pl.uuid.value, group.uuid)
        );
    }

    public handle_incoming_guess(player: PlayerUuid, contents: GameGuessWordRequest): void {
        const game = this.game_by_player(player.value);
        if (game !== null) {
            game.process_guess(player, contents.data.guess);
        }
    }

    public handle_player_left(player: PlayerUuid): void {
        const game = this.game_by_player(player.value);
        if (game) {
            game.connection_dropped(player);
        }
    }
}

export { GameHall };