import { GroupInfo } from "../../lib/src/data/group";
import { MIN_BLANKS, GameSettings, Game, CLOSE_DISTANCE, get_words, REVEAL_SEC } from "../../lib/src/data/game";
import { Uuid } from "../../lib/src/data/uuid";
import { ConnectionHandler } from "./connection_handler";
import { distance } from "fastest-levenshtein";
import { game_guess_correct_response, game_guess_false_response } from "../../lib/src/action/game_guess_word";
import { player_left_msg } from "../../lib/src/action/game_playerleft";
import { game_step_msg } from "../../lib/src/action/game_step";
import { game_done_msg } from "../../lib/src/action/game_done";

class GameHost implements Game {
    config: GameSettings;
    group: GroupInfo;
    score: Array<[Uuid, number]>;
    now_drawing: number;
    current_round: number;
    now_pattern: string;

    available_words: Set<string>;
    now_solution: string | null;

    ch: ConnectionHandler;
    done: boolean;

    reveal_timer: NodeJS.Timeout;

    constructor(config: GameSettings, group: GroupInfo, connection: ConnectionHandler) {
        const players = group.players.map(pl => pl.uuid);

        this.config = config;
        this.group = group;
        this.score = Array.from(players).map(item => [item, 0]);
        this.now_drawing = -1;
        this.current_round = 0;
        // Deep copy to make sure we retain standard categories.
        this.available_words = new Set(get_words(config.words));

        this.ch = connection;
        this.next();
    }

    public process_guess(player: Uuid, guess: string): void {
        const result = this.is_correct_guess(player, guess);

        let message = "";
        if (result === null) {
            message = JSON.stringify(game_guess_false_response(false));
            this.send_player(player, message);
        } else if (result === false) {
            message = JSON.stringify(game_guess_false_response(true));
            this.send_player(player, message);
        } else {
            message = JSON.stringify(game_guess_correct_response(player, this.now_solution));

            this.send_group(message);
            const index = this.score.findIndex(item => item[0].value === player.value);
            if (index != -1) {
                this.score[index][1] += 1;
            } else {
                console.log("player not part of game");
            }
            this.next();
        }
    }

    public connection_dropped(player: Uuid): void {
        const position = this.score.findIndex(item => item[0].value === player.value);

        if (position === -1) {
            // Player not in game.
        } else {
            // Delete the player from every aspect of our game.
            this.score.slice(position);
            if (this.now_drawing > position) {
                this.now_drawing = this.now_drawing + 1;
            } else if (this.now_drawing === position) {
                this.now_drawing = (this.now_drawing + 1) % this.score.length;
            }
        }
        this.group.remove_player(player);
        this.ch.remove(player);

        this.send_group(JSON.stringify(player_left_msg(player)));
    }

    // Returns whether the game is done. If this is the case, it can be deleted from the game_hall
    public can_be_stopped(): boolean {
        return this.done;
    }

    // Connection send helper functions

    private send_group(message: string): void {
        this.ch.send_group(this.group, message);
    }

    private send_player(player: Uuid, message: string): void {
        try {
            this.ch.get_connection(player).send(message);
        } catch (e) {
            // Connection lost?
            // TODO: handle?
        }
    }

    private send_guessers(message: string): void {
        this.group.players.forEach(player => {
            if (player.uuid.value !== this.score[this.now_drawing][0].value) {
                this.ch.get_connection(player.uuid)?.send(message);
            }
        });
    }

    /// Replaces one _ by a letter. Returns whether that was possible.
    private expand_pattern(): boolean {
        const amount_of_blanks = this.now_pattern.replace(/[^_]/g, "").length;

        if (amount_of_blanks < MIN_BLANKS) {
            return false;
        } else {
            let index;
            do { index = Math.floor(Math.random() * this.now_pattern.length); } while (this.now_pattern[index] !== "_");
            this.now_pattern = this.now_pattern.substr(0, index) + this.now_solution[index] + this.now_pattern.substr(index + 1);
            return true;
        }
    }

    private game_done(): void {
        this.done = true;
        this.send_group(JSON.stringify(game_done_msg(this.score)));
    }

    // Go to the next drawing or stop the game
    private next(): void {
        clearInterval(this.reveal_timer);
        this.reveal_timer = setInterval(() => {
            if (this.expand_pattern()) {
                const message = JSON.stringify(game_step_msg(this.as_game()));
                this.send_guessers(message);
            } else {
                clearInterval(this.reveal_timer);
            }
        }, REVEAL_SEC * 1000);

        this.now_drawing += 1;
        if (this.now_drawing >= this.score.length) {
            this.now_drawing = 0;
            this.current_round += 1;
        }
        if (this.current_round >= this.config.number_of_rounds) {
            // Done.
            this.game_done();
            return;
        }
        if (this.available_words.size === 0) {
            // No more words. We're done
            this.game_done();
            return;
        }

        // Select a new word
        const words = Array.from(this.available_words);
        const solution = words[Math.floor(Math.random() * words.length)];
        this.now_solution = solution.toUpperCase();
        this.available_words.delete(solution);
        this.now_pattern = this.now_solution.replace(/[A-Z0-9]/gi, "_");

        // inform connections 
        const message_guessers = JSON.stringify(game_step_msg(this.as_game()));
        this.score.forEach((player_score, idx) => {
            const player = player_score[0];
            if (idx === this.now_drawing) {
                /// Drawer
                this.send_player(player, JSON.stringify(game_step_msg(this.as_solution_game())));
            } else {
                /// Guesser
                this.send_player(player, message_guessers);
            }
        });
    }

    /// Returns true whwn correct, false when close and null when wrong.
    private is_correct_guess(player: Uuid, guess: string): boolean | null {
        guess = guess.toUpperCase();
        if (player.value === this.score[this.now_drawing][0].value) {
            // Drawer cannot guess
            return null;
        } else if (guess === this.now_solution) {
            return true;
        } else {
            return distance(guess.toUpperCase(), this.now_solution) < CLOSE_DISTANCE ? false : null;
        }
    }

    /// Converts this game into an object that is suitable to send to the different guessers.
    private as_game(): Game {
        return {
            group: this.group,
            score: this.score,
            config: this.config,
            current_round: this.current_round,
            now_drawing: this.now_drawing,
            now_pattern: this.now_pattern,
        };
    }

    /// Creates a game, but the pattern actually contains the solution. This is meant to be sent to the drawer only
    private as_solution_game(): Game {
        return {
            group: this.group,
            score: this.score,
            config: this.config,
            current_round: this.current_round,
            now_drawing: this.now_drawing,
            now_pattern: this.now_solution,
        };
    }
}


export { GameHost };