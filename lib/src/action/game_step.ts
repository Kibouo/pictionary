import { Game } from "../data/game";

const ACTION = "game_step";

interface GameStep {
    action: string,
    data: { game: Game },
}

function game_step_msg(game: Game): GameStep {
    return {
        action: ACTION,
        data: {
            game,
        }
    };
}

export { ACTION, GameStep, game_step_msg };
