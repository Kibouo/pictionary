import { Uuid } from "../data/uuid";

const ACTION = "ice_candidate_init";

interface IceCandidateInit {
    action: string,
    data: {
        candidate: RTCIceCandidateInit | RTCIceCandidate,
        target_user: Uuid
    }
}

function ice_candidate_init_msg(candidate: RTCIceCandidateInit | RTCIceCandidate, target_user: Uuid): IceCandidateInit {
    return {
        action: ACTION,
        data: {
            candidate: candidate,
            target_user: target_user
        }
    };
}

export { ACTION, IceCandidateInit, ice_candidate_init_msg };