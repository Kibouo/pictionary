import { Uuid } from "../data/uuid";

const ACTION = "player_left";

interface PlayerLeftAction {
    action: string,
    data: {
        player: Uuid,
    }
}

function player_left_msg(player: Uuid): PlayerLeftAction {
    return {
        action: ACTION,
        data: {
            player
        }
    };
}

export { ACTION, player_left_msg, PlayerLeftAction };