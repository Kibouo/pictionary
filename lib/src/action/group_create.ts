import { LobbyGroup, UnregisteredGroup } from "../data/group";
import { StatusReply, SUCCESS_ACTION } from "../data/status_reply";
import { Uuid } from "../data/uuid";

const ACTION = "group_create";


interface GroupCreate {
    action: string,
    data: {
        group: UnregisteredGroup,
    }
}

function group_create_msg(group: UnregisteredGroup): GroupCreate {
    return {
        action: ACTION,
        data: {
            group: group
        }
    };
}

/////////////////////////////////

interface GroupCreateReply extends StatusReply {
    data: {
        info: string,
        group?: Uuid,
        player?: Uuid,
    }
}

function group_create_reply(info: string, group?: Uuid, player?: Uuid): GroupCreateReply {
    return {
        action: ACTION,
        data: {
            info: info,
            group, player,
        }
    };
}

export {
    group_create_msg,
    ACTION,
    GroupCreate,
    group_create_reply,
    GroupCreateReply
};