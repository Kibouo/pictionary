import { Uuid } from "../data/uuid";

const ACTION = "sdp_offer_pass";

interface SdpOfferPass {
    action: string,
    data: {
        description: RTCSessionDescriptionInit,
        source_user: Uuid
    }
}

function sdp_offer_pass_msg(description: RTCSessionDescriptionInit, source_user: Uuid): SdpOfferPass {
    return {
        action: ACTION,
        data: {
            description: description,
            source_user: source_user
        }
    };
}

export { sdp_offer_pass_msg, SdpOfferPass, ACTION };