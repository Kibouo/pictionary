import { Uuid } from "../data/uuid";

const ACTION = "sdp_offer_init";

interface OfferInitDescription {
    content: RTCSessionDescriptionInit,
    target_user: Uuid
}

interface SdpOfferInit {
    action: string,
    data: {
        descriptions: OfferInitDescription[],
    }
}

function sdp_offer_init_msg(
    descriptions: OfferInitDescription[]
): SdpOfferInit {
    return {
        action: ACTION,
        data: {
            descriptions: descriptions,
        }
    };
}

export { sdp_offer_init_msg, SdpOfferInit, ACTION, OfferInitDescription };