import { status_reply_msg, StatusReply, SUCCESS_ACTION } from "../data/status_reply";
import { Uuid } from "../data/uuid";

const ACTION = "group_join";

interface GroupJoin {
    action: string,
    data: {
        group_uuid: Uuid,
        player_name: string
    }
}

function group_join_msg(group_uuid: Uuid, player_name: string): GroupJoin {
    return {
        action: ACTION,
        data: {
            group_uuid: group_uuid,
            player_name: player_name
        }
    };
}

/////////////////////////////////

interface GroupJoinReply extends StatusReply {
    data: {
        info: string,
        player?: Uuid,
    }
}

function group_join_reply(info = SUCCESS_ACTION, player?: Uuid): GroupJoinReply {
    return {
        action: ACTION,
        data: {
            info,
            player,
        }
    }
}

export {
    GroupJoin,
    group_join_msg,
    ACTION,
    GroupJoinReply,
    group_join_reply
};