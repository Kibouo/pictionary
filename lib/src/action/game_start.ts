import { Game } from "../data/game";
import { status_reply_msg, StatusReply, SUCCESS_ACTION } from "../data/status_reply";

const ACTION = "game_start";

interface GameStart {
    action: string,
    data: {
        info: string,
    }
}

function game_start_msg(status: string): GameStart {
    return {
        action: ACTION,
        data: {
            info: status,
        }
    };
}

export {
    GameStart,
    game_start_msg,
    ACTION,
};
