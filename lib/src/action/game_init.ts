import { status_reply_msg, StatusReply, SUCCESS_ACTION } from "../data/status_reply";

const ACTION = "game_init";

interface GameInit {
    action: string
}

function game_init_msg(): GameInit {
    return {
        action: ACTION
    };
}

/////////////////////////////////

type GameInitReply = StatusReply;

function game_init_reply(info: string): GameInitReply {
    return status_reply_msg(ACTION, info);
}

export {
    GameInit,
    game_init_msg,
    GameInitReply,
    game_init_reply,
    ACTION
};
