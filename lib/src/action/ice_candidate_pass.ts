import { Uuid } from "../data/uuid";

const ACTION = "ice_candidate_pass";

interface IceCandidatePass {
    action: string,
    data: {
        candidate: RTCIceCandidateInit | RTCIceCandidate,
        source_user: Uuid
    }
}

function ice_candidate_pass_msg(candidate: RTCIceCandidateInit | RTCIceCandidate, source_user: Uuid): IceCandidatePass {
    return {
        action: ACTION,
        data: {
            candidate: candidate,
            source_user: source_user
        }
    };
}

export { ACTION, IceCandidatePass, ice_candidate_pass_msg };