import { status_reply_msg, StatusReply, SUCCESS_ACTION } from "../data/status_reply";

const ACTION = "lobby_join";

interface LobbyJoin {
    action: string
}

function lobby_join_msg(): LobbyJoin {
    return {
        action: ACTION
    };
}

/////////////////////////////////

type LobbyJoinReply = StatusReply;

function lobby_join_reply(info = SUCCESS_ACTION): LobbyJoinReply {
    return status_reply_msg(ACTION, info);
}


export {
    LobbyJoin,
    lobby_join_msg,
    ACTION,
    LobbyJoinReply,
    lobby_join_reply
};