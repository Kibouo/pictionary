import { Group, GroupInfo } from "../data/group";
import { Uuid } from "../data/uuid";
import { StatusReply, SUCCESS_ACTION } from "../data/status_reply";

const ACTION = "lobby_list";

interface LobbyDiff {
    added: GroupInfo[],
    deleted: Uuid[]
}

interface LobbyList extends StatusReply {
    data: {
        info: string,
        added: Group[],
        deleted: Uuid[]
    }
}

function lobby_list_msg(diff: LobbyDiff, info = SUCCESS_ACTION): LobbyList {
    return {
        action: ACTION,
        data: {
            info: info,
            added: diff.added,
            deleted: diff.deleted
        }
    };
}

export { LobbyList, lobby_list_msg, ACTION, LobbyDiff };