import { Uuid } from "../data/uuid";

const ACTION = "sdp_reply_init";

interface SdpReplyInit {
    action: string,
    data: {
        description: RTCSessionDescriptionInit,
        target_user: Uuid
    }
}

function sdp_reply_init_msg(description: RTCSessionDescriptionInit, target_user: Uuid): SdpReplyInit {
    return {
        action: ACTION,
        data: {
            description: description,
            target_user: target_user
        }
    };
}

export { sdp_reply_init_msg, SdpReplyInit, ACTION };