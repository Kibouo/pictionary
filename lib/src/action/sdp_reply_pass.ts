import { Uuid } from "../data/uuid";

const ACTION = "sdp_reply_pass";

interface SdpReplyPass {
    action: string,
    data: {
        description: RTCSessionDescriptionInit,
        source_user: Uuid
    }
}

function sdp_reply_pass_msg(description: RTCSessionDescriptionInit, source_user: Uuid): SdpReplyPass {
    return {
        action: ACTION,
        data: {
            description: description,
            source_user: source_user
        }
    };
}

export { sdp_reply_pass_msg, SdpReplyPass, ACTION };