import { Uuid } from "../data/uuid";

const ACTION = "game_done";

interface GameDone {
    action: string,
    data: {
        final_scores: Array<[Uuid, number]>,
    }
}

function game_done_msg(final_scores: Array<[Uuid, number]>): GameDone {
    return { action: ACTION, data: { final_scores } };
}

export { ACTION, GameDone, game_done_msg };
