import { status_reply_msg, StatusReply, SUCCESS_ACTION } from "../data/status_reply";

const ACTION = "sdp_handshake";

type SdpHandshake = StatusReply;

function sdp_handshake_reply(info = SUCCESS_ACTION): SdpHandshake {
    return status_reply_msg(ACTION, info);
}

export {
    ACTION,
    SdpHandshake,
    sdp_handshake_reply
};
