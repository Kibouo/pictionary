import { Uuid } from "../data/uuid";

const ACTION = "game_guess_word";

interface GameGuessWordRequest {
    action: string,
    data: {
        guess: string,
    }
}

function game_guess_word_request(guess: string): GameGuessWordRequest {
    return {
        action: ACTION,
        data: {
            guess: guess
        }
    };
}

interface GameGuessWordFalseResponse {
    action: string,
    data: {
        close: boolean,
    }
}

function game_guess_false_response(close: boolean): GameGuessWordFalseResponse {
    return {
        action: ACTION,
        data: {
            close
        }
    };
}

// Broadcasted to all players
interface GameGuessWordCorrectResponse {
    action: string,
    data: {
        solution: string,
        player: Uuid,
    }
}

function game_guess_correct_response(player: Uuid, solution: string): GameGuessWordCorrectResponse {
    return {
        action: ACTION,
        data: {
            solution: solution,
            player,
        }
    };
}

export {
    GameGuessWordRequest,
    game_guess_word_request,
    GameGuessWordFalseResponse,
    GameGuessWordCorrectResponse,
    game_guess_false_response,
    game_guess_correct_response,
    ACTION
};
