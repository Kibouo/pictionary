function random_uint(max: number): number {
    if (max < 0) {
        throw new Error("random max is not uint");
    }

    return Math.floor(Math.random() * Math.floor(max));
}

function words_in_text(text: string): string[] {
    return text.split('\n').filter(item => item.length > 0);
}

export { random_uint, words_in_text };