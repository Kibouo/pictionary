import { Player } from "./player";
import { Uuid } from "./uuid";
import { v4 as uuidv4 } from "uuid";
import { GameSettings } from "./game";

const MIN_PLAYERS = 2;
const MAX_PLAYERS = 4;

interface UnregisteredGroup {
    name: string,
    leader: string,
    max_players: number,
    game_config: GameSettings
}

interface LobbyGroup extends Group {
    uuid: Uuid,
    name: string,
    leader: Player,
    max_players: number,
    other_players: Player[],
}

interface Group {
    uuid: Uuid,
    name: string,
    leader: Player,
    max_players: number
}

class GroupInfo implements Group {
    uuid: Uuid
    name: string
    leader: Player
    max_players: number
    game_config: GameSettings

    other_players: Player[]

    into_group(): Group {
        return {
            uuid: this.uuid,
            name: this.name,
            leader: this.leader,
            max_players: this.max_players,
        };
    }

    // Refuses to add duplicate member
    add_player(player: Player): boolean {
        if (this.other_players.length + 1 >= this.max_players) {
            return false;
        }
        if (this.leader.uuid.value === player.uuid.value
            || this.other_players.map(p => p.uuid.value).includes(player.uuid.value)) {
            return false;
        }

        this.other_players.push(player);

        return true;
    }

    // Refuses to remove leader
    remove_player(player_uuid: Uuid): boolean {
        if (this.leader.uuid === player_uuid) {
            return false;
        }

        const idx = this.other_players.map(p => p.uuid.value).indexOf(player_uuid.value);
        if (idx === -1) {
            return false;
        }

        this.other_players.splice(idx, 1);
        return true;
    }

    is_leader(player: Uuid): boolean {
        return this.leader.uuid.value === player.value;
    }

    get players(): Player[] {
        return [this.leader].concat(this.other_players);
    }

    constructor(name: string, leader: Player, max_players: number, game_config: GameSettings) {
        this.uuid = Uuid.parse(uuidv4()) as Uuid;
        this.name = name;
        this.leader = leader;
        this.max_players = Math.max(Math.min(max_players || 0, MAX_PLAYERS), MIN_PLAYERS);
        this.other_players = [];
        this.game_config = game_config;
    }
}

function get_player(group: GroupInfo, uuid: string): Player | undefined {
    if (group.leader.uuid.value === uuid) {
        return group.leader;
    } else {
        return group.other_players.find(player => player.uuid.value === uuid);
    }
}

export {
    UnregisteredGroup,
    LobbyGroup,
    Group,
    GroupInfo,
    get_player,
    MAX_PLAYERS,
    MIN_PLAYERS
};