import { Uuid } from "./uuid";

interface Player {
    name: string,
    uuid: Uuid
}

export { Player };