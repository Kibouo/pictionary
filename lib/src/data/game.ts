import { random_uint } from "../utils";
import { GroupInfo } from "./group";
import { Player } from "./player";
import { Uuid } from "./uuid";

const DEFAULT_POINTS = 1;
const MIN_BLANKS = 3;
const CLOSE_DISTANCE = 2;
const MIN_ROUNDS = 3;
const REVEAL_SEC = 10;

const DEFAULT_SETS = new Map();
const _MATH_SET = new Set();

_MATH_SET.add("triangle");
_MATH_SET.add("cube");
_MATH_SET.add("line");
_MATH_SET.add("point");
_MATH_SET.add("rectangle");
_MATH_SET.add("square");
_MATH_SET.add("hypercube");

DEFAULT_SETS.set("math", _MATH_SET);


const _TECH_SET = new Set();
_TECH_SET.add("hdmi");
_TECH_SET.add("www");
_TECH_SET.add("dns");
_TECH_SET.add("google");
_TECH_SET.add("usb");
_TECH_SET.add("recursion");
_TECH_SET.add("python");
_TECH_SET.add("cloudflare");

DEFAULT_SETS.set("tech", _TECH_SET);

// All methods that are used on the client, are not in a class. This is because we can only use them when they aren't members of a class, as they cannot be dynamically generated upon reading JSON.

interface GameSettings {
    number_of_rounds: number;
    /// Predefined list or a custom list.
    words: string | string[];
}

function default_settings(): GameSettings {
    return {
        number_of_rounds: MIN_ROUNDS,
        words: ["circle", "square", "rectangle", "cube"],
    };
}

function get_words(words: string | string[]): Set<string> {
    if (typeof words === "string") {
        return DEFAULT_SETS.get(words)!;
    } else {
        const set: Set<string> = new Set();
        words.forEach(item => set.add(item));
        return set;
    }
}

interface Game {
    group: GroupInfo;
    config: GameSettings;
    // In drawing order
    score: Array<[Uuid, number]>;
    now_drawing: number;
    current_round: number;
    now_pattern: string;
}

export { Game, GameSettings, default_settings, get_words, DEFAULT_POINTS, MIN_BLANKS, CLOSE_DISTANCE, DEFAULT_SETS, MIN_ROUNDS, REVEAL_SEC };
