import { validate, version } from "uuid";

class Uuid {
    value: string;

    private constructor(value: string) {
        this.value = value;
    }

    static parse(value: string): Uuid | null {
        return validate(value) ? new Uuid(value) : null;
    }

    version(): number {
        return version(this.value);
    }
}

export { Uuid };