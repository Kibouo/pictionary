const SUCCESS_ACTION = "OK";

interface StatusReply {
    action: string,
    data: {
        info: string
    }
}

function status_reply_msg(action: string, info = SUCCESS_ACTION): StatusReply {
    return {
        action: action,
        data: {
            info: info,
        }
    };
}

export { StatusReply, SUCCESS_ACTION, status_reply_msg };