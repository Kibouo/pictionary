import Vue from "vue";
import Vuetify from "vuetify/lib";
import colors from "vuetify/lib/util/colors";

Vue.use(Vuetify);

const customTheme = {
  themes: {
    light: {
      primary: colors.blue.darken3,
      secondary: colors.blue.darken3,
      accent: colors.lime.base
    },
    dark: {
      primary: colors.blue.darken3,
      secondary: colors.lightBlue.base,
      accent: colors.blueGrey.base
    }
  }
};

const vuetify = new Vuetify({
  theme: customTheme
});

export { vuetify };
