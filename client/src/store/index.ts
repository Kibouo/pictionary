import Vue from "vue";
import Vuex from "vuex";
import { ConnectionHandler } from "@/data/sockets/ConnectionHandler";
import { ServerConnection } from "@/data/sockets/ServerConnection";


import { Group, GroupInfo, LobbyGroup, MAX_PLAYERS, UnregisteredGroup } from "../../../lib/src/data/group";
import { Game } from "../../../lib/src/data/game";
import { Player } from "../../../lib/src/data/player";
import { lobby_join_msg } from "../../../lib/src/action/lobby_join";
import { group_create_msg, GroupCreateReply } from "../../../lib/src/action/group_create";
import { group_join_msg, GroupJoinReply } from "../../../lib/src/action/group_join";
import { SUCCESS_ACTION as GroupJoinSuccess } from "../../../lib/src/data/status_reply";
import { LobbyDiff } from '../../../lib/src/action/lobby_list';
import { Uuid } from '../../../lib/src/data/uuid';
import { OfferInitDescription, sdp_offer_init_msg } from '../../../lib/src/action/sdp_offer_init';
import { sdp_reply_init_msg } from '../../../lib/src/action/sdp_reply_init';
import { SdpOfferPass } from '../../../lib/src/action/sdp_offer_pass';
import { SdpReplyPass } from '../../../lib/src/action/sdp_reply_pass';
import { IceCandidatePass } from '../../../lib/src/action/ice_candidate_pass';
import { game_init_msg } from '../../../lib/src/action/game_init';
import { PlayerLeftAction } from '../../../lib/src/action/game_playerleft';
import { GameStep } from '../../../lib/src/action/game_step';
import { GameGuessWordFalseResponse, GameGuessWordCorrectResponse, game_guess_word_request } from "../../../lib/src/action/game_guess_word";
import { DataChannelInfo } from '@/data/sockets/PeerConnection';
import { GameDone } from '../../../lib/src/action/game_done';

Vue.use(Vuex);

// JS' `Map.has()` works by object comparison, we want value comparison tho so we can't use `Uuid`
type UuidValue = string;

interface AppState {
  admin: boolean;
  connectionHandler: ConnectionHandler | null;

  game: Game | null;

  group: LobbyGroup | null;
  group_uuid: string | null;
  group_created: UnregisteredGroup | null;

  lobby: GroupInfo[];
  loading: boolean;
  player: Player | string | null;
  players: Player[];

  notifier_service: ((message: string, type: string) => void) | null;
  overlay_service: ((title: string, message: string) => void) | null;

  trackHandlers: Map<UuidValue, (track_event: RTCTrackEvent) => any>;
  videoStreams: Map<UuidValue, MediaStream>;
  audioStreams: Map<UuidValue, MediaStream>;
  ourStreams: MediaStream[];
  dataChannels: DataChannelInfo;

  micMuted: boolean;
  cameraMuted: boolean;
  dataSaver: boolean;

  done: (() => void) | null;
}

const initialState: AppState = {
  admin: false,
  connectionHandler: null,
  player: null,
  players: [],

  game: null,

  group: null,
  group_uuid: null,
  group_created: null,

  lobby: [],
  loading: false,

  notifier_service: null,
  overlay_service: null,

  trackHandlers: new Map(),
  videoStreams: new Map(),
  audioStreams: new Map(),
  ourStreams: [],
  dataChannels: new Map(),

  micMuted: false,
  cameraMuted: false,
  dataSaver: false,

  done: null,
}

interface SendPeerPayload {
  channel_label: string,
  data: ArrayBuffer
}

export const store = new Vuex.Store<AppState>({
  state: initialState,
  mutations: {
    lobbyUpdate(state, payload: LobbyDiff) {
      // Remove gone games
      payload.deleted.forEach(deletedGroup => {
        const idx = state.lobby.findIndex(item => item.uuid.value === deletedGroup.value);

        if (idx >= 0)
          Vue.delete(state.lobby, idx);
      });

      // Add new games
      payload.added.forEach(addedGroup => {
        state.lobby.push(addedGroup);
      });

      // Vue shenanigans
      if (state.group_uuid !== null) {
        const uuid = state.group_uuid;

        const new_group = state.lobby.find((item) => item.uuid.value === uuid);
        state.group = new_group === undefined ? null : new_group;
        state.group_uuid = new_group?.uuid.value || null;
      }
    },
    setPlayerName(state, newPlayerName: string) {
      if (state.player === null) {
        state.player = newPlayerName;
      } else if (typeof state.player === "string") {
        state.player = newPlayerName;
      } else {
        state.player.name = newPlayerName;
      }
    },
    joinGroupResponse(state, payload: GroupJoinReply) {
      if (payload.data.info !== GroupJoinSuccess) {
        // Group must have left lobby or is not valid.
        state.group = null;
        state.group_uuid = null;
      } else {
        if (typeof state.player === "string" || state.player === null) {
          state.player = { name: state.player ? state.player : "anonymous", uuid: payload.data.player! }
        }
      }
    },

    createGroupResponse(state, payload: GroupCreateReply) {
      // Set the player UUID
      if (typeof state.player === "string") {
        state.player = { name: state.player, uuid: payload.data.player! };
      } else if (state.player === null) {
        state.player = { name: "anonymous", uuid: payload.data.player! };
      } else {
        // Player already set
        throw "setting player twice";
      }

      if (state.group_created === null) {
        throw "settings were not set";
      }

      state.group = {
        name: state.group_created.name, leader: state.player, max_players: state.group_created.max_players, uuid: payload.data.group!, other_players: []
      };
      state.group_uuid = payload.data.group!.value;

      state.player = { name: (state.player === null) ? "anonymous" : typeof state.player === "string" ? state.player : "anonymous", uuid: payload.data.player! };
    },

    playerLeft(state, payload: PlayerLeftAction) {
      // TODO
    },

    gameStep(state, step: GameStep) {
      Vue.set(state, "game", step.data.game);
    },

    gameGuessResponse(state, response: GameGuessWordFalseResponse | GameGuessWordCorrectResponse) {
      function gameGuessIsFalse(obj: any): obj is GameGuessWordFalseResponse {
        return obj.data.close !== undefined;
      }
      if (gameGuessIsFalse(response)) {
        if (response.data.close) {
          // We were close
          if (state.notifier_service !== null) {
            state.notifier_service("Close guess! Maybe a typo?", "orange darken-2");
          }
        } else {
          // We were wrong and not even close
          if (state.notifier_service !== null) {
            state.notifier_service("Wrong guess!", "red darken-4");
          }
        }
      } else {
        // Somebody had the right answer. Maybe it was us?
        if (state.game === null) {
          throw "gameIsNullHowDidWeGetHere?";
        }
        if (state.notifier_service !== null) {
          state.notifier_service(`${[state.game.group.leader].concat(state.game.group.other_players).find(item => item.uuid.value == response.data.player.value)!.name} was the first to guess the correct answer "${response.data.solution}"`, "green darken-3");
        }
      }
    },

    initMessageSvc(state, function_call: (message: string, type: string) => void) {
      state.notifier_service = function_call;
    },

    initGameDone(state, f: () => void) {
      state.done = f;
    },

    gameDone(state, payload: GameDone) {
      state.game!.score = payload.data.final_scores;
      if (state.done !== null) {
        state.done();
      }
    },

    _connection(state, connection: ConnectionHandler) {
      state.connectionHandler = connection;
    },
    _startLoading(state) {
      state.loading = true;
    },
    _stopLoading(state) {
      state.loading = false;
    }
  },
  actions: {
    async ensureServerConnection(store): Promise<void> {
      store.commit("_startLoading");
      store.commit("_connection", new ConnectionHandler(await new ServerConnection().init()));
      store.commit("_stopLoading");
    },

    async setPlayer(store, player: string): Promise<void> {
      const state = store.state;
      state.player = player;
      if (state.group !== null) {
        const ug = state.group as GroupInfo;
        store.commit("_startLoading");
        const message = group_join_msg(ug.uuid, ug.name);

        state.connectionHandler?.sendServer(JSON.stringify(message));

        store.commit("_stopLoading");
      }
    },

    async joinLobby(store): Promise<void> {
      const state = store.state;

      state.loading = true;
      const message = lobby_join_msg();
      state.connectionHandler?.sendServer(JSON.stringify(message));
      state.loading = false;
    },

    async joinGroupRequest(store, payload: Uuid): Promise<void> {
      store.commit("_startLoading");
      const state = store.state;
      state.admin = false;

      if (state.player === null) {
        throw "joining group without player setup";
      }
      if (state.group !== null) {
        throw "joining group while already in group";
      }


      if (typeof state.player !== "string") {
        // Maybe we were kicked before?
        const message = group_join_msg(payload, state.player.name);
        state.connectionHandler?.sendServer(JSON.stringify(message));
        state.group = state.lobby.find(item => item.uuid.value === payload.value) || null;
      } else {
        const message = group_join_msg(payload, state.player);
        state.connectionHandler?.sendServer(JSON.stringify(message));
      }
      state.group_uuid = payload.value;
      state.loading = false;
    },

    async createGroup(store, payload: UnregisteredGroup): Promise<void> {
      const state = store.state;
      state.admin = true;

      if (typeof state.player === "string") {
        const group = { ...payload, leader: state.player };
        const message = group_create_msg(group);
        await state.connectionHandler?.sendServer(JSON.stringify(message));

        // Add group to store
        state.group_created = group;
      }
    },

    async gameInit(store) {
      store.state.connectionHandler?.sendServer(JSON.stringify(game_init_msg()));
    },

    async generateTrackHandlers(store) {
      if (!store.state.group) {
        return;
      }

      (store.getters.otherPlayersOfGroup as Player[])
        .map(player => player.uuid)
        .forEach(uuid => {
          const videoStream = new MediaStream();
          const audioStream = new MediaStream();

          const handler = (event: RTCTrackEvent) => {
            console.log("trigger_handler: ", event);

            switch (event.track.kind) {
              case "video":
                videoStream.addTrack(event.track);
                break;

              case "audio":
                audioStream.addTrack(event.track);
                break;

              default:
                break;
            }
          };

          store.state.videoStreams.set(uuid.value, videoStream);
          store.state.audioStreams.set(uuid.value, audioStream);
          store.state.trackHandlers.set(uuid.value, handler);
        });
    },

    async registerStreams(store) {
      try {
        const stream = await navigator.mediaDevices.getUserMedia({
          video: true, audio: true
        });
        store.state.ourStreams.push(stream);

        console.log("register_own_stream: ", stream);
      } catch (error) {
        console.error(`Cannot access mic/camera: ${error.message}`)
      }
    },

    async generateDataChannelInfo(store) {
      store.state.dataChannels.set("draw", (_) => { throw new Error("UNIMPLEMENTED DRAW CHANNEL") });
    },

    async updateDataChannelInfo(store, payload: DataChannelInfo) {
      for (const key of payload.keys()) {
        store.state.dataChannels.set(key, payload.get(key)!);
      }
    },

    async sdpOfferInit(store) {
      await store.dispatch("registerStreams");
      await Promise.all([
        store.dispatch("generateTrackHandlers"),
        store.dispatch("generateDataChannelInfo"),
      ]);

      // We don't want double connections (both parties starting a connection to each other).
      // To prevent this, we let clients only send invites to "lower ranked" clients. 
      // The ranks are decided by alphabetic ordering of the UUID.
      const sorted_players =
        [store.state.group?.leader]
          .concat(store.state.group?.other_players)
          .filter(player => player)
          .sort((a, b) => a?.uuid.value! < b?.uuid.value! ? -1 : 1);
      const own_idx = sorted_players.findIndex(player =>
        player?.uuid.value === (store.state.player as Player)?.uuid.value);
      const other_players = sorted_players.slice(own_idx + 1);

      // I'd combine UUID and offer as a tuple, but who knows what JS decides
      // to do with this construct: `Promise.all([Promise, Uuid][])`
      const offer_promises: (Promise<RTCSessionDescriptionInit> | undefined)[] =
        other_players
          .map(other_player => {
            console.log("track_handler_init: ", store.state.trackHandlers.get(other_player!.uuid.value));

            return store.state.connectionHandler?.newPeerConnection(
              other_player!.uuid,
              store.state.ourStreams,
              store.state.trackHandlers.get(other_player!.uuid.value)!,
              store.state.dataChannels
            ).init();
          }) || [];

      const offers = await Promise.all(offer_promises);
      const descriptions: OfferInitDescription[] = [];
      for (let i = 0; i < offers.length; i++) {
        const player = other_players[i];
        const offer = offers[i];

        if (!player || !offer) {
          continue;
        }

        descriptions.push({ content: offer, target_user: player.uuid });
      }

      store.state.connectionHandler?.sendServer(JSON.stringify(
        sdp_offer_init_msg(descriptions)));
    },

    async sdpOfferProcess(store, payload: SdpOfferPass) {
      console.log("track_handler_process: ", store.state.trackHandlers.get(payload.data.source_user.value));

      const reply = await store
        .state.connectionHandler?.newPeerConnection(
          payload.data.source_user,
          store.state.ourStreams,
          store.state.trackHandlers.get(payload.data.source_user.value)!,
          store.state.dataChannels
        ).replyInit(payload.data.description);

      if (reply) {
        store.state.connectionHandler?.sendServer(JSON.stringify(
          sdp_reply_init_msg(reply, payload.data.source_user)));
      }
    },

    async sdpReplyProcess(store, payload: SdpReplyPass) {
      const conn = store.state.connectionHandler?.peerConnectionByUuid(payload.data.source_user);
      conn?.finishInit(payload.data.description);
    },

    async iceCandidateProcess(store, payload: IceCandidatePass) {
      const conn = store.state.connectionHandler?.peerConnectionByUuid(payload.data.source_user);
      conn?.addIceCandidate(payload.data.candidate);
    },

    async sendPeers(store, payload: SendPeerPayload) {
      store.state.connectionHandler?.sendPeers(payload.channel_label, payload.data);
    },

    async clearPeers(store) {
      store.state.connectionHandler?.clearPeers();
    },

    async sendGuess(store, payload: { guess: string }) {
      store.state.connectionHandler?.sendServer(JSON.stringify(game_guess_word_request(payload.guess)));
    },

    async toggleMic(store) {
      store.state.micMuted = !store.state.micMuted;

      const streams = store.state.ourStreams as MediaStream[];
      streams.forEach(stream =>
        stream
          .getAudioTracks()
          .forEach(track => (track.enabled = !track.enabled))
      );
    },

    async toggleCam(store) {
      store.state.cameraMuted = !store.state.cameraMuted;

      const streams = store.state.ourStreams as MediaStream[];
      streams.forEach(stream =>
        stream
          .getVideoTracks()
          .forEach(track => (track.enabled = !track.enabled))
      );
    },

    async toggleDataSaver(store) {
      store.state.dataSaver = !store.state.dataSaver;

      if (store.state.dataSaver !== store.state.cameraMuted) {
        store.dispatch("toggleCam");
      }
    },
  },
  getters: {
    lobbyGames(state): Group[] {
      return state.lobby;
    },
    isAdmin(state): boolean {
      return state.admin;
    },
    setupComplete(state): boolean {
      return state.game !== null;
    },
    players(state): Player[] {
      if (state.group === null) {
        return [];
      } else {
        return [state.group.leader].concat(state.group.other_players);
      }
    },
    adminPlayer(state): Player | null {
      if (state.group === null) {
        return null;
      } else {
        return state.group.leader;
      }
    },
    thisPlayer(state): Player | null {
      if (typeof state.player === "string") {
        return null;
      } else {
        return state.player;
      }
    },
    otherPlayersOfGroup(state): Player[] {
      const players = [state.group?.leader].concat(state.group?.other_players) || [];

      return (players as Player[])
        .filter(player => player?.uuid.value !== (state.player as Player | null)?.uuid?.value);
    },
    videoStreams(state): Map<string, MediaStream> {
      return state.videoStreams;
    },
    audioStreams(state): Map<string, MediaStream> {
      return state.audioStreams;
    },
    allowedToDraw(state): boolean {
      return state.game?.score[state.game?.now_drawing][0].value
        === (state.player as Player)?.uuid.value;
    }
  },
  modules: {}
});

export { SendPeerPayload };