import { store } from "@/store";

import { ACTION as LOBBY_LIST_ACTION } from "../../../../lib/src/action/lobby_list";
import { ACTION as LOBBY_JOIN_ACTION } from "../../../../lib/src/action/lobby_join";
import { ACTION as GROUP_JOIN_ACTION } from "../../../../lib/src/action/group_join";
import { ACTION as GROUP_CREATE_ACTION } from "../../../../lib/src/action/group_create";
import { ACTION as GAME_INIT_ACTION, GameInitReply } from "../../../../lib/src/action/game_init";
import { ACTION as SDP_OFFER_PASS_ACTION } from "../../../../lib/src/action/sdp_offer_pass";
import { ACTION as SDP_REPLY_PASS_ACTION } from "../../../../lib/src/action/sdp_reply_pass";
import { ACTION as ICE_CANDIDATE_PASS_ACTION } from "../../../../lib/src/action/ice_candidate_pass";
import { ACTION as PLAYER_LEFT_ACTION } from "../../../../lib/src/action/game_playerleft";
import { ACTION as SDP_HANDSHAKE_ACTION, SdpHandshake } from "../../../../lib/src/action/sdp_handshake";
import { ACTION as GAME_STEP_ACTION } from "../../../../lib/src/action/game_step";
import { ACTION as GAME_GUESS_ACTION } from "../../../../lib/src/action/game_guess_word";
import { ACTION as GAME_DONE_ACTION } from "../../../../lib/src/action/game_done";
import { SUCCESS_ACTION } from '../../../../lib/src/data/status_reply';

const MAX_ATTEMPTS = 3;

class ServerConnection {
    private _connection: WebSocket;
    private host?: string;
    private port?: number;
    private finished_sdp_init: Promise<any> | null = null;

    constructor(host?: string, port?: number) {
        const ws = new WebSocket(
            `wss://${host ? host : location.hostname}:${port ? port : location.port}`
        );
        ws.onopen = (): void => console.log("Hi server");
        // TODO: onclose should notify the user. We probably want to re-connect?
        ws.onclose = (): void => console.log("Bye server");
        ws.onmessage = async (event): Promise<void> => await this.handle_server_message(ws, event);
        ws.onerror = (error): void => console.error(error);

        this._connection = ws;
    }

    async init(): Promise<ServerConnection> {
        let attempts = 0;
        return new Promise((resolve, reject) => {
            const interval = setInterval(() => {
                if (this._connection.readyState === WebSocket.OPEN) {
                    clearInterval(interval);
                    resolve(this);
                } else if (attempts >= MAX_ATTEMPTS) {
                    reject(
                        new Error(
                            `Failed to connect to "wss://${this.host ? this.host : location.hostname}:${this.port ? this.port : location.port
                            }" after ${MAX_ATTEMPTS} attempts.`
                        )
                    );
                }

                attempts++;
            }, 1500);
        });
    }

    private async handle_server_message(ws: WebSocket, event: MessageEvent): Promise<void> {
        if (event.data instanceof ArrayBuffer || event.data instanceof Blob) {
            console.log("bin data");
        } else if (typeof event.data === "string") {
            const contents = JSON.parse(event.data);

            switch (contents.action) {
                case LOBBY_LIST_ACTION:
                    store.commit("lobbyUpdate", contents.data);
                    break;

                case GROUP_JOIN_ACTION:
                    store.commit("joinGroupResponse", contents);
                    break;

                case GAME_INIT_ACTION:
                    if ((contents as GameInitReply).data
                        && (contents as GameInitReply).data.info !== SUCCESS_ACTION) {
                        store.dispatch("clearPeers");
                    } else {
                        this.finished_sdp_init = store.dispatch("sdpOfferInit");
                    }
                    break;

                case SDP_OFFER_PASS_ACTION:
                    await this.finished_sdp_init;
                    store.dispatch("sdpOfferProcess", contents);
                    break;

                case SDP_REPLY_PASS_ACTION:
                    store.dispatch("sdpReplyProcess", contents);
                    break;

                case ICE_CANDIDATE_PASS_ACTION:
                    store.dispatch("iceCandidateProcess", contents);
                    break;

                case LOBBY_JOIN_ACTION:
                    break;

                case GROUP_CREATE_ACTION:
                    store.commit("createGroupResponse", contents);
                    break;

                case PLAYER_LEFT_ACTION:
                    store.commit("playerLeft", contents);
                    break;

                case SDP_HANDSHAKE_ACTION:
                    if ((contents as SdpHandshake).data.info !== SUCCESS_ACTION) {
                        store.dispatch("clearPeers");
                    }
                    break;

                case GAME_STEP_ACTION:
                    store.commit("gameStep", contents);
                    break;

                case GAME_GUESS_ACTION:
                    store.commit("gameGuessResponse", contents);
                    break;

                case GAME_DONE_ACTION:
                    store.commit("gameDone", contents);
                    break;

                default:
                    ws.dispatchEvent(new CustomEvent('error', { detail: new Error(`Invalid API action: ${JSON.stringify(contents)}`) }));
            }
        } else {
            ws.dispatchEvent(new CustomEvent('error', { detail: new Error(`Invalid message type: ${event.data}`) }));
        }
    }

    send(
        data: string | ArrayBuffer | SharedArrayBuffer | Blob | ArrayBufferView
    ): void {
        this._connection.send(data);
    }

    get connection(): WebSocket {
        return this._connection;
    }
}

export { ServerConnection };