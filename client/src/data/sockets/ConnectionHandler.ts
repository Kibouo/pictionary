import { Uuid } from '../../../../lib/src/data/uuid';
import { DataChannelInfo, PeerConnection } from "./PeerConnection";
import { ServerConnection } from "./ServerConnection";
import { ice_candidate_init_msg } from "../../../../lib/src/action/ice_candidate_init";
import { game_start_msg } from "../../../../lib/src/action/game_start";
import { SUCCESS_ACTION } from '../../../../lib/src/data/status_reply';


// JS' `Map.has()` works by object comparison, we want value comparison tho so we can't use `Uuid`
type PlayerUuidValue = string;

class ConnectionHandler {
    private serverConnection: ServerConnection;
    private _peerConnections: Map<PlayerUuidValue, PeerConnection> = new Map();
    private establishedPeerConnections: number = 0;

    constructor(serverConnection: ServerConnection) {
        this.serverConnection = serverConnection;
    }

    get peerConnections(): PeerConnection[] {
        return [...this._peerConnections.values()];
    }

    peerConnectionByUuid(uuid: Uuid): PeerConnection | undefined {
        return this._peerConnections.get(uuid.value);
    }

    newPeerConnection(
        uuid: Uuid,
        streams: MediaStream[],
        handler: (track_event: RTCTrackEvent) => any,
        data_channels: DataChannelInfo
    ): PeerConnection {
        const peer = new PeerConnection(
            streams,
            handler,
            (event) => {
                if (event.candidate) {
                    this.sendServer(JSON.stringify(ice_candidate_init_msg(event.candidate, uuid)))
                }
            },
            () => {
                // TODO: there could be error checking here to ensure
                // all peers are connected within x seconds.
                this.establishedPeerConnections++;
                if (this.establishedPeerConnections == this.peerConnections.length) {
                    this.sendServer(JSON.stringify(game_start_msg(SUCCESS_ACTION)));
                }
            },
            () => {
                // TODO: should probably be an "ABORT GAME" message or whatever
                this.sendServer(JSON.stringify(game_start_msg("Client failed to connect.")));
            },
            data_channels
        );
        this._peerConnections.set(uuid.value, peer);
        return peer;
    }

    sendServer(
        message: string | ArrayBuffer | SharedArrayBuffer | Blob | ArrayBufferView
    ): void {
        this.serverConnection.send(message);
    }

    sendPeers(channel_label: string, data: ArrayBuffer) {
        this.peerConnections.forEach(connection => connection.sendData(channel_label, data));
    }

    clearPeers(): void {
        this._peerConnections.clear();
        this.establishedPeerConnections = 0;
    }
}

export { ConnectionHandler };
