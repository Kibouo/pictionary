const PEER_CONNECTION_CONFIG = {
    iceServers: [
        {
            urls: ["stun:stun.l.google.com:19302"],
        },
    ],
};

type DataChannelInfo = Map<string, (event: MessageEvent) => any>;

// A very important feature of RTC connections is that clients have to re-negotiate on a change.
// In practice this means that when streams are added or properties of a stream are changed,
// the setup process starting from the `createOffer` is repeated.
// We did not know this before-hand, hence the following code might not be flexible enough to
// handle a re-negotiation. Sadly it's too close to the deadline to fix this.
// We did learn this the hard way (trying to add streams after a connection was established).
// It gave us a lot of stress, so we won't forget ;)  
class PeerConnection {
    private connection: RTCPeerConnection;
    private data_channels: Map<string, RTCDataChannel> = new Map();
    private ice_queue: (RTCIceCandidateInit | RTCIceCandidate)[] = [];

    constructor(
        streams: MediaStream[],
        track_handler: (track_event: RTCTrackEvent) => any,
        handle_ice_candidate: (ice_event: RTCPeerConnectionIceEvent) => any,
        handle_connection_established: () => void,
        handle_connection_failed: () => void,
        data_channels: DataChannelInfo
    ) {
        this.connection = new RTCPeerConnection(PEER_CONNECTION_CONFIG);

        this.connection.ontrack = track_handler;
        this.connection.onicecandidate = handle_ice_candidate;
        // NOTE: does not work on Firefox 82.0
        this.connection.onconnectionstatechange = (_) => {
            console.log("conn_state: ", this.connection.connectionState)
            switch (this.connection.connectionState) {
                case "connected":
                    handle_connection_established();
                    break;

                case "failed":
                    handle_connection_failed();
                    break;

                default:
                    break;
            }
        }

        // Add media streams
        streams.forEach(stream =>
            stream.getTracks().forEach(track => this.connection.addTrack(track, stream)));

        // Add data channels
        for (let i = 0; i < data_channels.size; i++) {
            const label = [...data_channels][i][0];

            const data_channel = this.connection.createDataChannel(
                label, { negotiated: true, id: i });
            data_channel.onmessage = (event) => {
                // dynamically call handler as we hacked it to change after a while. 
                // This was such that we could add a generic handler first and change it to a 
                // working handler bound to the `GameView`.
                // Otherwise, we would have had to re-negotiate to add data channels 
                // *after* the connection is made (which happens before entering `GameView`).
                // This is ugly and we should just implement re-negotiation next time.
                const handler = data_channels.get(label)!;
                handler(event);
            };

            this.data_channels.set(label, data_channel);
        }
    }

    public async init(): Promise<RTCSessionDescriptionInit> {
        const offer = await this.connection.createOffer();
        await this.connection.setLocalDescription(offer);
        return offer;
    }

    public async replyInit(offer: RTCSessionDescriptionInit): Promise<RTCSessionDescriptionInit> {
        await this.connection.setRemoteDescription(offer);
        const answer = await this.connection.createAnswer();
        await this.connection.setLocalDescription(answer);
        return answer;
    }

    public async finishInit(offer: RTCSessionDescriptionInit): Promise<void> {
        await this.connection.setRemoteDescription(offer);

        for (const candidate of this.ice_queue) {
            this.connection.addIceCandidate(candidate);
        }
    }

    public async addIceCandidate(candidate: RTCIceCandidateInit | RTCIceCandidate): Promise<void> {
        if (!this.connection || !this.connection.remoteDescription?.type) {
            this.ice_queue.push(candidate);
        } else {
            this.connection.addIceCandidate(candidate);
        }
    }

    public sendData(channel_label: string, data: ArrayBuffer) {
        const channel = this.data_channels.get(channel_label);
        channel?.send(data);
    }
}

export { PeerConnection, DataChannelInfo };
