import Vue, { VNode } from "vue";
import App from "./App.vue";
import { store } from "./store";
import { vuetify } from "./plugins/vuetify";

Vue.config.productionTip = false;

new Vue({
  render: (h): VNode => h(App),
  vuetify,
  store
}).$mount("#app");
