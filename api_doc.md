# Server WebSocket API
For game control data, the server expects UTF8 JSON data. I.e. stringified JS objects.

## Data structures
Data structures used to represent items which re-appear all throughout the API.

### Uuid
A UUID string. We use v4.
```json
{
    value: string,
}
```

### Player
Basic information on a player.
```json
{
    name: string,
    uuid: Uuid
}
```

### UnregisteredGroup
Basic information on a group, to be filled out by a client. It lacks a UUID as this is chosen by the server.
```json
{
    name: string,
    leader: string,
    max_players: number
}
```

### Group
Basic information on a group. Note: compared `UnregisteredGroup`, the `leader` is now a `Player` object. 
```json
{
    uuid: Uuid,
    name: string,
    leader: Player,
    max_players: number
}
```

### GroupInfo
The full information on a `Group`. Note: the leader is not included in the list of `other_players`.
```json
{
    uuid: Uuid,
    name: string,
    leader: Player,
    other_players: Player[],
    max_players: number
}
```

## StatusReply
The basis of a response. All replies to a request extend this message type. Contains the action to which it responds, as well as an `info` string. If everything is OK, this contains `OK`. 

The `info` property will further not be mentioned.
```json
{
    action: string,
    data: {
        info: string,
    }
}
```

## API actions
Compared to REST-APIs, there is no path with web-sockets. To indicate actions, the `action` field is used.

### Client
Possible data sent by the clients to the server.

#### LobbyJoin
Sets user/connection to be in the lobby. Right after joining the lobby, the server will reply with a `LobbyList` of all open groups. After having joined, clients receive periodic updates on changes made to the groups in the lobby. These updates will stop once they start playing a game.
```json
{
    action: "lobby_join"
}
```

#### GroupCreate
Registers a group in the lobby. I.e. a game in wait for players to join. It will be included in the next periodic `LobbyList` update. Clients which joined a group are expected to get information on it from this list.
```json
{
    action: "group_create",
    data: {
        group: UnregisteredGroup,
    }
}
```

#### GroupJoin
Attempts to set user/connection to be part of the group they request. The player name will be the name shown to other players.
```json
{
    action: "group_join",
    data: {
        group_uuid: Uuid,
        player_name: string
    }
}
```

The server replies with a `StatusReply`.

### Server
Possible data sent by the server to the clients.

#### LobbyList
Lists the diff of the list of lobbies, compared to the previous update sent by the server.
```json
{
    action: "lobby_list",
    data: {
        added: Group[]
        deleted: Uuid[]
    }
}
```
