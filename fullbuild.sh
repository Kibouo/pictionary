#!/bin/sh
cd client
npm i &
cd ../server
npm i &
cd ../lib
npm i &
cd ../server
cd ../client
wait
npm run build &
cd ../server
npm run build &
wait
echo "Done, start the server with \`cd server && npm run start\`"
echo "Other required steps:"
echo " - setup a certificate with gen_cert.sh"
echo " - put the trusted host (WS_HOST) in .env"
